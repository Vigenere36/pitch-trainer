#include "pitchTrainer.h"

//Helper methods

void printNote(unsigned char note) {
	std::string keys[] = {"C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"};
	int key = note % 12;
	int octaves = abs(floor((float)(note - 60) / 12));
	std::string description = note < 60 ? "below" : "above";

	std::cout << "Note: " << keys[key] << ", " << octaves << " octaves " << description << " middle C\n";
}

//RtMidiIn Callbacks

void printInput(double deltatime, std::vector<unsigned char>* message, void* userData) {
	unsigned int nBytes = message->size();
	if (nBytes == 3 && message->at(0) == 0x90)
		printNote(message->at(1));
	/*for (unsigned int i = 0; i < nBytes; i++)
		std::cout << "Byte " << i << " = " << (int)message->at(i) << ", ";
	if (nBytes > 0)
		std::cout << "stamp = " << deltatime << std::endl;*/
}

void setRange(double deltatime, std::vector<unsigned char>* message, void* userData) {
	PitchTrainer* trainer = (PitchTrainer*) userData;
	if (message->size() == 3 && message->at(0) == 0x90) {
		if (message->at(1) < trainer->lowNote) trainer->lowNote = message->at(1);
		if (message->at(1) > trainer->highNote) trainer->highNote = message->at(1);
	}
}

void checkNote(double deltatime, std::vector<unsigned char>* message, void* userData) {
	PitchTrainer* trainer = (PitchTrainer*) userData;
	unsigned char randomNote = trainer->randomNote;
	unsigned char randomNote3 = randomNote + 3;
	unsigned char randomNote4 = randomNote + 4;
	unsigned char randomNote7 = randomNote + 7;

	if (message->size() == 3 && message->at(0) == 0x90) {
		if (message->at(1) == randomNote) { //Play major chord
			std::vector<unsigned char> key1 = {0x90, randomNote, 90};
			std::vector<unsigned char> key3 = {0x90, randomNote4, 90};
			std::vector<unsigned char> key5 = {0x90, randomNote7, 90};
			std::vector<unsigned char> key1up = {0x80, randomNote, 90};
			std::vector<unsigned char> key3up = {0x80, randomNote4, 90};
			std::vector<unsigned char> key5up = {0x80, randomNote7, 90};
			trainer->midiout->sendMessage(&key1);
			trainer->midiout->sendMessage(&key3);
			trainer->midiout->sendMessage(&key5);
			usleep(150000);
			trainer->midiout->sendMessage(&key1);
			trainer->midiout->sendMessage(&key3);
			trainer->midiout->sendMessage(&key5);
			usleep(750000);
			trainer->midiout->sendMessage(&key1up);
			trainer->midiout->sendMessage(&key3up);
			trainer->midiout->sendMessage(&key5up);
			trainer->midiout->sendMessage(&key1up);
			trainer->midiout->sendMessage(&key3up);
			trainer->midiout->sendMessage(&key5up);
			trainer->playRandomNote();
		} else {
			std::vector<unsigned char> key1 = {0x90, randomNote, 90};
			trainer->midiout->sendMessage(&key1);
		}
	}
}

//Class methods

void PitchTrainer::displayInput(void) const {
	int nPorts = midiin->getPortCount();
	std::cout << "There are " << nPorts << " MIDI input sources available.\n";
	std::string portName;
	for (int i = 0; i < nPorts; i++) {
		try {
			portName = midiin->getPortName(i);
		} catch (RtMidiError &error) {
			perror("Error: Invalid port. Exiting.");
		}
		std::cout << " Input Port #" << i+1 << ": " << portName << '\n';
	}
}

void PitchTrainer::getKeyboardRange(void) {
	midiin->setCallback(&setRange, this);
	std::cout << "\nPlease press the lowest and highest keys on your keyboard. Press <enter> when finished.\n";
	char input;
	std::cin.get(input);
	midiin->cancelCallback();
}

void PitchTrainer::printKeyboardRange(void) {
	//Assuming middle C is 60
	std::string keys[] = {"C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"};
	int lowKey = lowNote % 12;
	int highKey = highNote % 12;
	int lowOctaves = abs(floor((float)(lowNote - 60) / 12));
	int highOctaves = abs(floor((float)(highNote - 60) / 12));
	std::string lowDescription = lowNote < 60 ? "below" : "above";
	std::string highDescription = highNote < 60 ? "below" : "above";

	std::cout << "Keyboard range:\n";
	std::cout << "Low note: " << keys[lowKey] << ", " << lowOctaves << " octaves " << lowDescription << " middle C\n";
	std::cout << "High note: " << keys[highKey] << ", " << highOctaves << " octaves " << highDescription << " middle C\n";
}

void PitchTrainer::playRandomNote(void) {
	randomNote = rand() % (highNote - lowNote + 1) + lowNote;

	//Check for whole note if applicable
	bool wholeNotes[] = {1,0,1,0,1,1,0,1,0,1,0,1};
	if (wholeNotes && !wholeNotes[randomNote % 12]) playRandomNote();

	std::vector<unsigned char> keyDown = {0x90, randomNote, 90};
	midiout->sendMessage(&keyDown);
}

void PitchTrainer::runTrainer(void) {
	displayInput();

	//Detect keyboard range
	while (lowNote > highNote) {
		getKeyboardRange();
	}
	printKeyboardRange();

	//Ask for whole notes
	char input;
	while (input != 'y' && input != 'n') {
		std::cout << "\nWhole notes only? Enter 'y' or 'n'.\n";
		std::cin.get(input);
	}
	if (input == 'y') wholeNotes = true;

	midiin->setCallback(&checkNote, this);
	playRandomNote();

	while(true) {
		usleep(10000000);
	}
}