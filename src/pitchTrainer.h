#include <iostream>
#include <cstdlib>
#include <unistd.h>
#include <vector>
#include <string>
#include <cmath>
#include "../lib/RtMidi/RtMidi.h"

class PitchTrainer {
public:
	RtMidiIn* midiin;
	RtMidiOut* midiout;
	unsigned char highNote, lowNote, randomNote;
	bool wholeNotes;

	PitchTrainer(RtMidiIn* _midiin, RtMidiOut* _midiout):
	midiin(_midiin),
	midiout(_midiout)
	{
		highNote = 0;
		lowNote = 255;
		randomNote = 0;
		wholeNotes = false;
	}

	~PitchTrainer(void) {
		delete midiin;
		delete midiout;
	}

	void displayInput(void) const;
	void getKeyboardRange(void);
	void printKeyboardRange(void);
	void playRandomNote(void);
	void runTrainer(void);
};