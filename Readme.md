##MidiTrainer

###Overview

This program allows you to practice a piece by loading in a midi file and allowing a note to play if it matches.

To run, first compile the midifile library by running 'make' in the lib/midifile directory, then compile by running 'make' on root directory