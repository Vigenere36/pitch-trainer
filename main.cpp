#include "lib/RtMidi/RtMidi.h"
#include "src/pitchTrainer.h"

int main() {
	RtMidiIn* midiin = 0;
	RtMidiOut* midiout = 0;

	try {
		midiin = new RtMidiIn();
		midiout = new RtMidiOut();
	} catch (RtMidiError &error) {
		error.printMessage();
		exit(EXIT_FAILURE);
	}

	int nPorts = midiin->getPortCount();
	if (!nPorts) {
		std::cout << "No MIDI input available\n";
		delete midiin;
		delete midiout;
		return 0;
	}

	midiin->openPort(0);
	midiout->openPort(0);

	PitchTrainer trainer(midiin, midiout);
	trainer.runTrainer();
}