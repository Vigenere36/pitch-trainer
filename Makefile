CC = g++

CFLAGS = -c -O2 -pthread -std=c++0x

LFLAGS = -O2 -pthread -std=c++0x \
		-framework CoreMIDI -framework CoreAudio -framework CoreFoundation #Required by RtMidi

TARGET = build/run

LIB = lib/RtMidi/librtmidi.a \
	lib/midifile/lib/libmidifile.a

SRCS  := $(wildcard src/*.cpp)
OBJS  := $(SRCS:.cpp=.o)

RM = /bin/rm -rf

all: $(TARGET)

$(TARGET): $(OBJS) main.cpp
	$(CC) $(LFLAGS) $(OBJS) main.cpp -o $(TARGET) $(LIB)

.cpp.o:
	$(CC) $(CFLAGS) $< -o $@

check:
	@$(CC) $(LFLAGS) $(OBJS) ./test/main.cpp -o ./test/test
	@echo "\n...test begin...\n"
	@./test/test input.txt
	@rm -f test/test
	@echo "...test end...\n"

clean: 
	$(RM) $(OBJS) $(TARGET)
